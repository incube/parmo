
var map;
var lat = 45.433892;
var lng = -73.649607;
var latlng;
var infoWindow;
var zoom = 15;

$(document).ready(function() {


//CARTE GOOGLE
	if(typeof google != 'undefined' && google){
		latlng = new google.maps.LatLng(lat, lng);
		myOptions = {
			zoom: zoom,
			center: latlng
		};
		var conteneur = document.getElementById("carte");
		map = new google.maps.Map(conteneur, myOptions);
		textAddress = $('.contact address').html();
		var bulle = '<div class="bulle"><h3>Le Groupe Parmo</h3>'+textAddress+'</div>';

		var monMarker = new google.maps.Marker({
			position: latlng,
			map: map,
			bulle: bulle
		});
		google.maps.event.addListener(monMarker, 'click', function() {
			if (infoWindow)
				infoWindow.close();
			infoWindow = new google.maps.InfoWindow({
				content: this.bulle
			});
			infoWindow.open(map, this);
		});
	}

//TOGGLE MENU
	$('.menu-trigger').click(function(event) {
		event.preventDefault();
		$(this).toggleClass('active');
		$('.navToggle').toggleClass('active');
	});

});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIl9tYWluLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6Im1haW4uanMiLCJzb3VyY2VzQ29udGVudCI6WyJcbnZhciBtYXA7XG52YXIgbGF0ID0gNDUuNDMzODkyO1xudmFyIGxuZyA9IC03My42NDk2MDc7XG52YXIgbGF0bG5nO1xudmFyIGluZm9XaW5kb3c7XG52YXIgem9vbSA9IDE1O1xuXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbigpIHtcblxuXG4vL0NBUlRFIEdPT0dMRVxuXHRpZih0eXBlb2YgZ29vZ2xlICE9ICd1bmRlZmluZWQnICYmIGdvb2dsZSl7XG5cdFx0bGF0bG5nID0gbmV3IGdvb2dsZS5tYXBzLkxhdExuZyhsYXQsIGxuZyk7XG5cdFx0bXlPcHRpb25zID0ge1xuXHRcdFx0em9vbTogem9vbSxcblx0XHRcdGNlbnRlcjogbGF0bG5nXG5cdFx0fTtcblx0XHR2YXIgY29udGVuZXVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjYXJ0ZVwiKTtcblx0XHRtYXAgPSBuZXcgZ29vZ2xlLm1hcHMuTWFwKGNvbnRlbmV1ciwgbXlPcHRpb25zKTtcblx0XHR0ZXh0QWRkcmVzcyA9ICQoJy5jb250YWN0IGFkZHJlc3MnKS5odG1sKCk7XG5cdFx0dmFyIGJ1bGxlID0gJzxkaXYgY2xhc3M9XCJidWxsZVwiPjxoMz5MZSBHcm91cGUgUGFybW88L2gzPicrdGV4dEFkZHJlc3MrJzwvZGl2Pic7XG5cblx0XHR2YXIgbW9uTWFya2VyID0gbmV3IGdvb2dsZS5tYXBzLk1hcmtlcih7XG5cdFx0XHRwb3NpdGlvbjogbGF0bG5nLFxuXHRcdFx0bWFwOiBtYXAsXG5cdFx0XHRidWxsZTogYnVsbGVcblx0XHR9KTtcblx0XHRnb29nbGUubWFwcy5ldmVudC5hZGRMaXN0ZW5lcihtb25NYXJrZXIsICdjbGljaycsIGZ1bmN0aW9uKCkge1xuXHRcdFx0aWYgKGluZm9XaW5kb3cpXG5cdFx0XHRcdGluZm9XaW5kb3cuY2xvc2UoKTtcblx0XHRcdGluZm9XaW5kb3cgPSBuZXcgZ29vZ2xlLm1hcHMuSW5mb1dpbmRvdyh7XG5cdFx0XHRcdGNvbnRlbnQ6IHRoaXMuYnVsbGVcblx0XHRcdH0pO1xuXHRcdFx0aW5mb1dpbmRvdy5vcGVuKG1hcCwgdGhpcyk7XG5cdFx0fSk7XG5cdH1cblxuLy9UT0dHTEUgTUVOVVxuXHQkKCcubWVudS10cmlnZ2VyJykuY2xpY2soZnVuY3Rpb24oZXZlbnQpIHtcblx0XHRldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXHRcdCQodGhpcykudG9nZ2xlQ2xhc3MoJ2FjdGl2ZScpO1xuXHRcdCQoJy5uYXZUb2dnbGUnKS50b2dnbGVDbGFzcygnYWN0aXZlJyk7XG5cdH0pO1xuXG59KTsiXSwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=