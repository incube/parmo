
var map;
var lat = 45.433892;
var lng = -73.649607;
var latlng;
var infoWindow;
var zoom = 15;

$(document).ready(function() {


//CARTE GOOGLE
	if(typeof google != 'undefined' && google){
		latlng = new google.maps.LatLng(lat, lng);
		myOptions = {
			zoom: zoom,
			center: latlng
		};
		var conteneur = document.getElementById("carte");
		map = new google.maps.Map(conteneur, myOptions);
		textAddress = $('.contact address').html();
		var bulle = '<div class="bulle"><h3>Le Groupe Parmo</h3>'+textAddress+'</div>';

		var monMarker = new google.maps.Marker({
			position: latlng,
			map: map,
			bulle: bulle
		});
		google.maps.event.addListener(monMarker, 'click', function() {
			if (infoWindow)
				infoWindow.close();
			infoWindow = new google.maps.InfoWindow({
				content: this.bulle
			});
			infoWindow.open(map, this);
		});
	}

//TOGGLE MENU
	$('.menu-trigger').click(function(event) {
		event.preventDefault();
		$(this).toggleClass('active');
		$('.navToggle').toggleClass('active');
	});

});