/********************************/
/* Version    : 1.2     */
/* lastModif  : René Domingue */
/* date     : 2015/06/05  */
/********************************/

var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var newer = require('gulp-newer');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var minifyCss = require('gulp-minify-css');
var notify = require("gulp-notify");
var bower = require('gulp-bower');
var livereload = require('gulp-livereload');

var config = {
  bowerDir: './assets_dev/vendor'
}
var paths = {
  src: 'assets_dev',
  dest: 'assets_dist',
  jssrc: './assets_dev/js/**/*.js',
  csss: './assets_dev/css/**/*.scss',
  imgSrc: './assets_dev/img/**/*',
  imgDest: './assets_dist/img'
};
gulp.task('bower', function() {
    return bower()
      .pipe(gulp.dest(config.bowerDir))
});

gulp.task('concatTaskBuild', function() {
  return gulp.src(paths.jssrc)
    .pipe(newer(paths.jssrc))
    .pipe(sourcemaps.init())
    .pipe(uglify())
    .pipe(concat('main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets_dist/js'));
});

gulp.task('concatTask', function() {
  return gulp.src(paths.jssrc)
    .pipe(newer(paths.jssrc))
    .pipe(sourcemaps.init())
    .pipe(concat('main.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets_dist/js'))
    .pipe(livereload());
});

gulp.task('concatTaskVendor', function() {
  return gulp.src(['./assets_dev/vendor/jquery/dist/jquery.min.js', './assets_dev/vendor/bootstrap-sass-official/assets/javascripts/bootstrap.min.js'])
    .pipe(sourcemaps.init())
    .pipe(concat('vendor.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./assets_dist/js'));
});

gulp.task('imgTask', function() {
  return gulp.src(paths.imgSrc)
    .pipe(newer(paths.imgDest))
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    }))
        .pipe(gulp.dest(paths.imgDest))
        .pipe(livereload());
});

gulp.task('sassTask', function () {
  gulp.src('./assets_dev/css/**/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    .on('error', gutil.log)
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./assets_dist/css'))
    .pipe(livereload());
});

// Rerun the task when a file changes
gulp.task('watchTask', function() {
  livereload.listen();
  gulp.watch(paths.jssrc, ['concatTask']);
  gulp.watch(paths.imgSrc, ['imgTask']);
  gulp.watch(paths.csss, ['sassTask']);
});

gulp.task('default', ['watchTask','concatTaskVendor', 'sassTask', 'concatTask', 'imgTask']);
gulp.task('build', ['sassTask','concatTaskVendor', 'concatTaskBuild', 'imgTask']);